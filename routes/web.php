<?php

use App\Http\Livewire\Auth\RegisterComponent;
use App\Http\Livewire\Calender\CalanderComponent;
use App\Http\Livewire\Car\CarComponent;
use App\Http\Livewire\Categories\CategoryComponent;
use App\Http\Livewire\Categories\MedicalCategoryComponent;
use App\Http\Livewire\Dashboard\DashboardComponent;
use App\Http\Livewire\Departments\DepartmentsComponent;
use App\Http\Livewire\Doctors\Detailcomponent;
use App\Http\Livewire\Doctors\DoctorsComponent;
use App\Http\Livewire\Patients\PatientDetailComponent;
use App\Http\Livewire\Patients\PatientsComponent;
use App\Http\Livewire\Procedure\ProcedureComponent;
use App\Http\Livewire\Rfid\AssignRfidCardComponent;
use App\Http\Livewire\Rfid\RfidCardComponent;
use App\Http\Livewire\Roles\RoleAssignmentComponent;
use App\Http\Livewire\Roles\RoleComponent;
use App\Http\Livewire\Sehat\SehatPatientRecordComponent;
use App\Http\Livewire\Services\ServicesComponent;
use App\Http\Livewire\Staff\StaffComponent;
use App\Http\Livewire\User\ConsultantComponent;
use App\Http\Livewire\User\UserCreateComponent;
use App\Models\Sehat\SehatPatientRecord;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function ()
// {
//     // // return view('auth.login');
//     // return redirect()->to('login');
// });


Auth::routes();

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('/', DashboardComponent::class)->name('dashboard');

    // patients
    Route::group(['prefix' => 'category'], function ()
    {
        Route::get('/', CategoryComponent::class)->name('category');
    });


    Route::group(['prefix' => 'car'], function ()
    {
        Route::get('/', CarComponent::class)->name('car');
    });
});
