
## Test job API
The JobTest API is a web service which allows you to use our platform from your website, software or mobile application. The API gives you access to all of the supported functions of our service.

## API Documetation  
https://documenter.getpostman.com/view/15262127/Uz5NjCxQ

## Test Api
To Test API please add mailing configurations in .env file e.g mailtrap etc

## Development 
This Application provide Web portal and API Endpoints.
Web portal build on Livewire.
For datatable we use Livewire PowerGrid Package.

## Migration 
Migrate application with seeds "php artisan migrate --seed"
Test User 
Email : admin@test.com
Password : password


