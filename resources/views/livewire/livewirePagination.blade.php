@if ($data->hasPages())
    <div class="mobile-pagination-responsive">
        <nav class="row justify-content-md-center justify-content-left ml-1 ml-md-0">

            {{ $data->links('livewire::bootstrap') }}

        </nav>
    </div>
@endif
