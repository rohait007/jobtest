<div class="card">

    <div class="card-header">
        <h3> Register {{ $type }}</h3>
    </div>
    {{-- <hr> --}}
    <div class="card-body">
        <form class="row g-3" wire:submit.prevent="save">
            <div class="col-12 col-md-6 col-xxl-4 mx-auto ">
                <div class="col-md-12">
                    <label for="inputEmail4" class="form-label">Name</label>
                    <input wire:model.defer='input.name' type="text" class="form-control" placeholder="" id="validationCustom01" autofocus>

                    @error('input.name')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>
                <div class="col-md-12">
                    <label for="inputEmail4" class="form-label">Email</label>
                    <input wire:model.debounce.800ms='email' type="email" class="form-control" placeholder="info@example.com" id="inputEmail4">
                    @error('input.email')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                    @if ($emailExist)
                        <div class="text-danger">{{ 'User already exist againts this email' }}</div>
                    @endif
                </div>
                <div class="col-md-12">
                    <label for="cnic" class="form-label">CNIC</label>
                    <input wire:model.debounce.800ms='cnic' type="number" class="form-control" placeholder="37301XXXXXXXX" id="cnic">
                    @error('input.cnic')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                    @if ($cnicAssigned)
                        <div class="text-danger">{{ 'User already exist againts this Cnic' }}</div>
                    @endif
                </div>
                <div class="col-md-12">
                    <label for="inputPassword4" class="form-label">Card</label>
                    <input wire:model.debounce.500ms='rfidToken' type="number" class="form-control" placeholder="Scan Card" id="inputPassword4">
                    @error('input.rfid_id')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                    @if ($rfidTokenAssigned)
                        <div class="text-danger">{{ 'User already exist againts this Rfid Card' }}</div>
                    @endif

                </div>
                <div class="row mt-5">
                    <div class="d-flex justify-content-center">
                        <button type="submit" class="btn btn-primary" {{ !$emailExist && !$rfidTokenAssigned && !$cnicAssigned == true ? '' : 'disabled' }}>Submit</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

</div>
