<div>
    <div class="row mb-2">
        <div class="col-md-4 offset-md-8">
            <div class="float-end">
                @if ($createAllow)
                    <x-add-btn wire:click="$set('createAllow', false)">Back</x-add-btn>
                @else
                    <x-add-btn wire:click='showCreate'>Add</x-add-btn>
                @endif
            </div>
        </div>
    </div>

    @if ($createAllow)
        {{-- creation Form --}}
        <div class="card p-3">
            <div class="row">
                <div class="col-12 col-md-8 mx-auto">
                    <form class="g-3" wire:submit.prevent="save">
                        <div class="row my-4">
                            <div class="">
                                <label for="name" class="form-label">{{ $heading }} Name</label>
                                <input wire:model.defer='model.name' type="text" class="form-control" placeholder="Name" required>

                                @error('model.name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row my-4">
                            <div class="">
                                <label for="name" class="form-label">Department</label>
                                <select wire:model.defer='model.department_id' class="form-control rounded-0" required>
                                    <option value="">
                                        {{ '--Select Department--' }}
                                    </option>
                                    @forelse ($departments as $department)
                                        <option value="{{ $department->id }}">
                                            {{ $department->name ?? 'N/A' }}
                                        </option>
                                    @empty
                                        <option>
                                            {{ 'No deparment. Please create department First' }}
                                        </option>
                                    @endforelse
                                </select>
                                @error('model.department_id')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            @if ($departments->count() > 0)
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            @else
                                <div class="d-flex justify-content-end">
                                    <a href="{{ route('department') }}" class="btn btn-warning">Go to deparment </a>
                                </div>
                            @endif
                        </div>

                    </form>
                </div>

            </div>
        </div>
    @else
        {{-- data --}}
        <x-table.table>
            <x-slot name="heading">{{ $heading }} </x-slot>

            <x-slot name='head'>
                <x-table.heading>{{ $heading }} Name </x-table.heading>
                <x-table.heading>Department</x-table.heading>
                <x-table.heading>Actions</x-table.heading>

            </x-slot>
            <x-slot name='body'>
                {{-- Search Row --}}
                <x-table.row>
                    <x-table.cell col='4'>
                        <x-inputSearchText model='name' placeholder='Name'></x-inputSearchText>
                    </x-table.cell>
                    <x-table.cell>
                    </x-table.cell>
                    <x-table.cell>
                    </x-table.cell>
                </x-table.row>

                @forelse ($data as $row)
                    <x-table.row>
                        <x-table.cell>{{ $row->name ?? 'N/A' }}</x-table.cell>
                        <x-table.cell>{{ $row->department->name ?? 'N/A' }}</x-table.cell>
                        <x-table.cell>
                            <div class="d-flex">
                                <x-edit-btn wire:click='edit({{ $row->id }})' href="#" data-bs-toggle="modal" data-bs-target="#livewireModal">Edit</x-edit-btn>
                                {{-- <x-del-btn wire:click='destroy({{ $row->id }})'>Delete</x-del-btn> --}}

                            </div>

                        </x-table.cell>
                    </x-table.row>
                @empty
                    @include('livewire.noRecordFound')
                @endforelse
            </x-slot>

        </x-table.table>

        <div class="mx-auto">
            @include('livewire.livewirePagination', ['data' => $data])
        </div>
    @endif


    <!-- Modal -->
    <form class="" wire:submit.prevent="update">

        <div wire:ignore.self class="modal fade" id="livewireModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="">
                                <label for="name" class="form-label">Procedure Name</label>
                                <input wire:model.defer='model.name' type="text" class="form-control" placeholder="Name" required>

                                @error('model.name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row my-2">
                            <div class="">
                                <label for="name" class="form-label">Department</label>
                                <select wire:model.defer='model.department_id' class="form-control rounded-0" required>
                                    <option value="">
                                        {{ '--Select Department--' }}
                                    </option>
                                    @forelse ($departments as $department)
                                        <option value="{{ $department->id }}">
                                            {{ $department->name ?? 'N/A' }}
                                        </option>
                                    @empty
                                        <option>
                                            {{ 'No deparment. Please create department First' }}
                                        </option>
                                    @endforelse
                                </select>
                                @error('model.department_id')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

    </form>


</div>
