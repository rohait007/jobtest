<div>
    <div class="row mb-2">
        <div class="col-md-4 offset-md-8">
            <div class="float-end">
                @if ($createAllow)
                    <x-add-btn wire:click="$set('createAllow', false)">Back</x-add-btn>
                @else
                    <x-add-btn wire:click='showCreate'>Add</x-add-btn>
                @endif
            </div>
        </div>
    </div>

    @if ($createAllow)
        {{-- creation Form --}}
        <div class="card p-3">


            <div class="row">
                <div class="col-12 col-md-8 mx-auto">
                    <form class="g-3" wire:submit.prevent="{{ $route }}">
                        <div class="row my-4">
                            <div class="">
                                <label for="name" class="form-label">{{ $heading }} Name</label>
                                <input wire:model.defer='model.name' type="text" class="form-control" placeholder="Name" required>

                                @error('model.name')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>

            </div>


        </div>
    @else
        @if ($poll)
            <div wire:poll.1ms="reloadDataTable">

            </div>
        @endif

        <div class="card p-3">
            @if ($showDataTable)
                @livewire('category-table')
            @endif
        </div>


    @endif

</div>
