<div>
    <div class="">
        <div class="row">
            <div class="col">
                <div class="page-description">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 mx-auto">
                <div class="card widget widget-stats">
                    <div class="card-body">
                        <div class="widget-stats-container d-flex">
                            <div class="widget-stats-icon widget-stats-icon-primary">
                                <i class="material-icons-outlined">directions_car</i>
                            </div>
                            <div class="widget-stats-content flex-fill">
                                <span class="widget-stats-title">Cars</span>
                                <span class="widget-stats-amount">{{ $data['total_cars'] }}</span>
                                <span class="widget-stats-info">Total number of registered cars</span>
                            </div>
                            {{-- <div class="widget-stats-indicator widget-stats-indicator-negative align-self-start">
                            <i class="material-icons">keyboard_arrow_down</i> 4%
                        </div> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
