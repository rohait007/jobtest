<a {{ $attributes->merge(['class' => 'btn btn-sm mt-1 mr-1']) }}>
    {{-- <span class="d-none d-sm-inline"></span> --}}
    {{ $slot }}
</a>
