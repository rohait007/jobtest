{{-- <thead>
    <tr>
        <th scope="col">#Id</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Action</th>

    </tr>
</thead> --}}

<th scope="col" {{ $attributes->merge(['class' => 'nowrap fit']) }}>{{ $slot }}</th>
