<div class="row mb-1">
    <div class="col-12 col-md-12 my-auto">
        <div class="row">
            <div class="col-10">
                <h3 class="">
                    {{ $heading ?? '' }}
                </h3>
            </div>
            <div class="col-2">
                <div wire:loading class="spinner-border text-primary float-end" role="status">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive" style="overflow-y:clip !important;">

    <table {{ $attributes->class(['table table-bordered table-striped bg-white ']) }}>
        <thead class="">
            <tr>
                {{ $head ?? '' }}
            </tr>
        </thead>
        <tbody>
            {{ $body ?? '' }}
        </tbody>
    </table>
</div>
