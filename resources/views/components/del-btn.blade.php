 <button onclick="confirm('Are you sure you want to remove?') || event.stopImmediatePropagation()" {{ $attributes->merge(['class' => 'btn btn-sm btn-danger']) }}>
     {{ $slot }}
     {{-- <x-icon class="fas fa-trash"></x-icon> --}}
     {{-- <span class="d-none d-sm-inline {{ $slot == '' ? '' : 'ml-1' }}"> {{ $slot }}</span> --}}
 </button>
