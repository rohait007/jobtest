<div class="mx-auto w-75 alert alert-{{ $type }}">
    {{ $message }}
</div>
