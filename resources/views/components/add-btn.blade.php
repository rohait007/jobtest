 <button {{ $attributes->merge(['class' => 'btn btn-sm btn-primary']) }}>
     <span> {{ $slot }}</span>
 </button>
