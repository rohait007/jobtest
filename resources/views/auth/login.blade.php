@extends('layouts.secondLayout')

@section('content')
    <div class="app app-auth-sign-in align-content-stretch d-flex flex-wrap justify-content-end">



        <div class="app-auth-background" style="">
        </div>

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="app-auth-container">
                @include('alerts.alerts')

                <div class="logo">
                    <a href="/">{{ env('APP_NAME', 'JobTest') }}</a>
                </div>
                <p class="auth-description">Please sign-in to your account and continue to the dashboard.<br>Don't have an account? <a href="{{ route('register') }}">Sign Up</a></p>

                <div class="auth-credentials m-b-xxl">

                    <div class="row mb-2">
                        <label for="signInEmail" class="form-label px-0">Email address</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@neptune.com" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                    <div class="row">
                        <label for="signInPassword" class="form-label px-0">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" required autocomplete="current-password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>



                </div>

                <div class=" auth-submit">
                    <div class="row">
                        <div class="col-12 col-md-6 px-0">
                            <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Sign In') }}
                            </button>

                        </div>
                        <div class="col-12 col-md-6 px-0">
                            @if (Route::has('password.request'))
                                <a class="auth-forgot-password float-end" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif

                        </div>

                    </div>
                </div>
                <div class="divider"></div>
                <div class="auth-alts">
                    <a href="#" class="auth-alts-google"></a>
                    <a href="#" class="auth-alts-facebook"></a>
                    <a href="#" class="auth-alts-twitter"></a>
                </div>
            </div>
        </form>
    </div>
@endsection
