@if ($notification = Session::get('success'))
    <div class="alert alert-success alert-block">
        <strong>{{ $notification }}</strong>
        {{ Session::forget('success') }}
    </div>
@endif
