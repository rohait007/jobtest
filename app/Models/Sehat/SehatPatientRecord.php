<?php

namespace App\Models\Sehat;

use App\Models\ConsultantVisit;
use App\Models\MedicalCategory;
use App\Models\Procedure;
use App\Models\Services;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SehatPatientRecord extends Model
{
    use HasFactory;

    public function procedure()
    {
        return $this->belongsTo(Procedure::class, 'procedure_id');
    }
    public function service()
    {
        return $this->belongsTo(Services::class, 'service_id');
    }
    public function category()
    {
        return $this->belongsTo(MedicalCategory::class, 'category_id');
    }
    public function consultant()
    {
        return $this->belongsTo(User::class, 'consultant_id');
    }

    public function functiongetTableColumns()
    {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function consultantPivotKeys()
    {
        return $this->hasMany(ConsultantVisit::class, 'file_id', 'id');
    }

    public function vistToConsultants()
    {
        return $this->belongsToMany(User::class, 'consultant_visits', 'file_id', 'consultant_id');
    }
}
