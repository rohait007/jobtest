<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LoginResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(Request $request)
    {

        $rules = array(
            'email' => 'email|required',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails())
        {
            return response(array(
                'message' => $validator->errors(),
            ), 400);
        }
        else
        {
            $loginDetails = request(['email', 'password']);
            if (!auth()->attempt($loginDetails))
            {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'password' => [
                            'Invalid credentials'
                        ],
                    ]
                ], 422);
            }

            $user = User::where('email', $request->email)->first();
            $token = $user->createToken('auth-token')->plainTextToken;

            return response()->json([
                'token' => $token,
            ])->setStatusCode(200);
        }
    }
}
