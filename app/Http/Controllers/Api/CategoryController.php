<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CatgegoryResource;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    protected $rules = [
        'name' => 'required|regex:/^[ A-Za-z0-9]+$/u' // exppression Allow space but no special char
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page_size = $request->page_size ?? 10;
        return (new CatgegoryResource(Category::paginate($page_size)))
            ->response()
            ->setStatusCode(200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), $this->rules);


        if ($validator->fails())
        {
            return response(array(
                'message' => $validator->errors(),
            ), 400);
        }
        else
        {
            $model =  Category::firstOrCreate(
                ['name' => $request->name]
            );

            if ($model)
            {
                return (new CatgegoryResource($model))
                    ->response()
                    ->setStatusCode(200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Category::find($id);
        if (!$model)
        {
            return response(array(
                'message' => 'No Category Found',
            ), 404);
        }
        else
        {
            return (new CatgegoryResource($model))
                ->response()
                ->setStatusCode(200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $model = Category::find($id);
        if (!$model)
        {
            return response(array(
                'message' => 'Not Catgory Found',
            ), 404);
        }


        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails())
        {
            return response(array(
                'message' => $validator->errors(),
            ), 400);
        }
        else
        {
            if ($model->update($request->all()))
            {
                return (new CatgegoryResource($model))
                    ->response()
                    ->setStatusCode(200);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Category::find($id);
        if (!$model)
        {
            return response(array(
                'message' => 'Not Catgory Found',
            ), 410);
        }

        if ($model->delete())
        {
            return (new CatgegoryResource($model))
                ->response()
                ->setStatusCode(200);
        }
    }
}
