<?php

namespace App\Http\Livewire\User;

use App\Models\Department;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class ConsultantComponent extends Component
{

    use WithPagination;

    public $heading = "Consultant";
    public $createAllow = false;
    public $searchInput = [];
    public $departments = [];
    public User $model;

    public function mount()
    {
        $this->createNewInstance();
        $this->departments = Department::get();
    }


    protected $rules = [
        'model.name' => 'required',
        'model.department_id' => 'required',
    ];

    public function createNewInstance()
    {
        // dd('here');
        $this->model = new User();
    }

    public function render()
    {
        return view('livewire.user.consultant-component')
            ->with(
                [
                    'data' => $this->getData()
                ]
            )
            ->extends('layouts.app', ['activePage' => 'consultant']);;
    }

    public function getData()
    {
        return User::Role('doctor')
            ->orderByDesc('updated_at')
            ->when($this->searchInput['name'] ?? false, function ($q)
            {
                $q->where('name', 'like', search($this->searchInput['name']));
            })
            ->with('department')
            ->paginate(10);
    }

    public function showCreate()
    {
        $this->createAllow = true;
        $this->createNewInstance();
    }

    public function save()
    {
        // dd($this->model);

        $this->validate();

        $this->model->save();
        $this->model->assignRole('doctor');

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Created',
        ]);

        $this->resetExcept('model', 'departments');
    }

    public function edit(User $model)
    {
        $this->model = $model;
    }
    public function update()
    {
        $this->validate();
        $this->model->update();
        $this->emit('closeModal');
    }

    public function destroy(User $model)
    {
        $model->delete();
    }
}
