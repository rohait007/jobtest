<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use App\Traits\RfidCardTrait;
use App\Traits\UserTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithPagination;

class UserCreateComponent extends Component
{
    public $type = null;

    use WithPagination;
    use UserTrait, RfidCardTrait;

    // public $employees = [];
    public $addEmp = false;
    public $input = [];
    public $email,  $emailExist = false;
    public $rfidToken,  $rfidTokenAssigned = false;
    public $cnic,  $cnicAssigned = false;
    public $rfidTokenForUpdate,  $rfidTokenAssignedForUpdate = false;

    public $selectedUserID = null;
    public $search = '';

    protected $rules = [
        'input.name' => 'required',
        'input.email' => 'required|email',
        'input.cnic' => 'required',
        'input.rfid_id' => 'required',
    ];

    protected $messages = [
        'input.name.required' => 'The Name cannot be empty.',
        'input.email.required' => 'The Email Address cannot be empty.',
        'input.password.required' => 'The Name cannot be empty.',
        'input.rfid_id.required' => 'The Name cannot be empty.',

        'input.email.email' => 'The Email Address format is not valid.',
    ];



    public function mount(Request $request)
    {
        $this->type = $request->type;
    }

    public function render()
    {
        return view('livewire.user.user-create-component')
            ->extends('layouts.app', ['activePage' => 'patient']);
    }

    public function updatedCnic()
    {
        // dd('here');
        if ($this->checkUserExistCnic($this->cnic))
        {
            $this->cnicAssigned = true;
        }
        else
        {
            $this->cnicAssigned = false;
        }
    }
    public function updatedEmail()
    {
        // dd('here');
        if ($this->checkUserExist($this->email))
        {
            $this->emailExist = true;
        }
        else
        {
            $this->emailExist = false;
        }
    }

    public function updatedRfidToken()
    {
        // dd('here');
        $token = $this->rfidToken;

        if ($this->CheckCardAssignToOtherUser($token))
        {
            $this->rfidTokenAssigned = true;
        }
        else
        {
            $this->rfidTokenAssigned = false;
        }
    }


    public function save()
    {
        // dd(redirect()->to($route));

        // dd('here');
        $this->input['email'] = $this->email;
        $this->input['cnic'] = $this->cnic;

        $token = $this->rfidToken;

        if (!$this->CheckCardExist($token))
        {
            $this->createRfidCard($token);
        }

        if ($this->CheckCardAssignToOtherUser($token))
        {
            $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'error',
                'title' => 'Error',
                'text' => 'Card Already Assigned',
            ]);
            return;
        }

        $card = $this->getRfidCard($this->rfidToken);
        // dd($card);
        if ($card != null)
        {
            $this->input['rfid_id'] = $card->id;
        }


        $this->validate();

        $this->input['password'] = Hash::make('hospital');

        $user =  User::create($this->input);
        $user->assignRole($this->type);

        // $this->dispatchBrowserEvent('swal:modal', [
        //     'type' => 'success',
        //     'title' => 'Success',
        //     'text' => $this->type . ' created',
        // ]);
        $route = $this->type . 's';

        $this->reset();


        return redirect()->to($route);
    }
}
