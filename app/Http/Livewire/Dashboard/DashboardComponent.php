<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\Car;
use Livewire\Component;

class DashboardComponent extends Component
{
    public $data;
    public function mount()
    {
        $this->data['total_cars'] = Car::count();
    }
    public function render()
    {
        return view('livewire.dashboard.dashboard-component')
            ->extends('layouts.app', ['activePage' => 'dashboard']);
    }
}
