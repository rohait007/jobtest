<?php

namespace App\Http\Livewire\Search;

use Livewire\Component;

class SearchComponent extends Component
{


    public $search = '';
    public function render()
    {
        return view('livewire.search.search-component');
    }

    public function updatedSearch()
    {
        $this->emit('searchListener', $this->search);
    }
}
