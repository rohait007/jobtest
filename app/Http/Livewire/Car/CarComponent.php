<?php

namespace App\Http\Livewire\Car;

use App\Models\Car;
use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;

class CarComponent extends Component
{
    use WithPagination;

    public $heading = "Car";
    public $createAllow = false;
    public $poll = false;
    public $showDatatable = true;
    public $categories;
    public $route = 'save';

    public Car $model;

    public function mount()
    {
        $this->createNewInstance();
    }


    protected $listeners = [
        'edit' => 'edit',
        'destroy' => 'destroy'
    ];


    protected $rules = [
        'model.category_id' => 'required',
        'model.color'       => 'required|regex:/^[ A-Za-z0-9]+$/u',
        'model.model'       => 'required|regex:/^[ A-Za-z0-9]+$/u',
        'model.reg_number'  => 'required|regex:/^[ A-Za-z0-9]+$/u',
    ];

    public function createNewInstance()
    {
        // dd('here');
        $this->model = new Car();
        $this->categories = Category::get();
    }

    public function render()
    {
        return view('livewire.car.car-component')
            ->extends('layouts.app', ['activePage' => 'Car']);
    }

    public function getData()
    {
        return Car::orderByDesc('updated_at')
            ->when($this->searchInput['name'] ?? false, function ($q)
            {
                $q->where('name', 'like', search($this->searchInput['name']));
            })
            ->paginate(10);
    }

    public function showCreate()
    {
        $this->createAllow = true;
        $this->createNewInstance();
    }

    public function save()
    {

        $this->validate();

        $this->model->save();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Created',
        ]);

        $this->resetExcept('model');
    }

    public function edit(Car $model)
    {
        // dd($model);
        $this->route = 'update';
        $this->model = $model;
        $this->createAllow = true;
    }

    public function update()
    {
        $this->validate();

        $this->model->update();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Record Updated',
        ]);

        $this->reset('createAllow');
    }

    public function destroy(Car $model)
    {
        $this->showDatatable = false;
        $this->poll = true;

        $model->delete();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Record Deleted',
        ]);
    }


    public function reloadDataTable()
    {
        if ($this->poll)
        {
            $this->poll = false;
        }

        $this->showDatatable = true;
    }
}
