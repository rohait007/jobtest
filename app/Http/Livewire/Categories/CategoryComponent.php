<?php

namespace App\Http\Livewire\Categories;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;

class CategoryComponent extends Component
{


    use WithPagination;

    public $heading = "Category";
    public $createAllow = false;
    public $poll = false;
    public $showDataTable = true;
    public $route = 'save';
    public $searchInput = [];
    public Category $model;

    public function mount()
    {
        $this->createNewInstance();
    }


    protected $listeners = [
        'edit' => 'edit',
        'destroy' => 'destroy'
    ];

    protected $rules = [
        'model.name' => 'required|regex:/^[ A-Za-z0-9]+$/u', // exppression Allow space but no special char
    ];


    public function createNewInstance()
    {
        // dd('here');
        $this->model = new Category();
    }

    public function render()
    {
        return view('livewire.categories.category-component')
            ->with(
                ['']
            )
            ->extends('layouts.app', ['activePage' => 'Category']);
    }

    public function getData()
    {
        return Category::orderByDesc('updated_at')
            ->when($this->searchInput['name'] ?? false, function ($q)
            {
                $q->where('name', 'like', search($this->searchInput['name']));
            })
            ->paginate(10);
    }

    public function showCreate()
    {
        $this->createAllow = true;
        $this->createNewInstance();
    }

    public function save()
    {
        // dd('here');

        $this->validate();

        $this->model->save();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Created',
        ]);

        $this->resetExcept('model');
    }

    public function edit(Category $model)
    {
        // dd($model);
        $this->route = 'update';
        $this->model = $model;
        $this->createAllow = true;
    }
    public function update()
    {
        $this->validate();

        $this->model->update();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Record Updated',
        ]);

        $this->reset('createAllow');
    }

    public function destroy(Category $model)
    {
        $this->showDataTable = false;
        $this->poll = true;

        $model->delete();

        $this->dispatchBrowserEvent('swal:modal', [
            'type' => 'success',
            'title' => 'Success',
            'text' => 'Record Deleted',
        ]);
    }

    public function reloadDataTable()
    {
        if ($this->poll)
        {
            $this->poll = false;
        }

        $this->showDataTable = true;
    }
}
