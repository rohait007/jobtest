<?php

namespace App\Http\Livewire\ViewMode;

use Illuminate\Support\Facades\Session;
use Livewire\Component;
use phpDocumentor\Reflection\Types\Void_;

class ViewModeComponent extends Component
{
    public $darkMode;
    public function mount()
    {
        if (Session::has('darkMode'))
        {
            $this->darkMode = Session::get('darkMode');
        }
        else
        {
            $this->darkMode = false;
        }
        // $this->darkMode = Session::has('darkMode') ? Session::get('darkMode') : true;
    }
    public function render()
    {
        return view('livewire.view-mode.view-mode-component')
            ->extends('layouts.app');
    }

    public function switchMode()
    {
        // dd(Session::get('darkMode'));
        if ($this->darkMode)
        {
            Session::put('darkMode', false);
        }
        else
        {
            Session::put('darkMode', true);
        }
        return redirect(request()->header('Referer'));
    }
}
