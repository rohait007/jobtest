<?php

/**
 * LaraClassified - Classified Ads Web Application
 * Copyright (c) BedigitCom. All Rights Reserved
 *
 * Website: https://bedigit.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Traits;

use App\Models\Attendance;
use App\Models\RfidCard;
use App\Models\UserLogs;
use App\Models\UserOvertime;
use Carbon\Carbon;
use Hamcrest\Type\IsInteger;
use Illuminate\Support\Facades\Auth;

trait RfidCardTrait
{

    public function CheckCardExist($token)
    {
        $card = RfidCard::where('token', $token)->first();
        if ($card == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function createRfidCard($token)
    {
        $card = new RfidCard();
        $card->token = $token;
        $card->non_zero_token = ltrim($token, "0");
        $card->save();
        return true;
    }


    public function getRfidCard($token)
    {
        return RfidCard::where('token', $token)->first();
    }


    public function CheckNonZeroCardExist($token)
    {
        $card = RfidCard::where('non_zero_token', $token)->first();
        if ($card == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function CheckCardAssignToOtherUser($token)
    {
        $card = RfidCard::where('token', $token)->first();
        if ($card == null)
        {
            return false;
        }

        if ($card->user == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function SavingUserPositionAndLog($user)
    {
        if ($user->position == 'IN')
        {
            $this->CreatingUserOutnLog($user->national_id);
            $user->position = 'OUT';
            $user->save();
        }
        elseif ($user->position == 'OUT')
        {
            $this->CreatingUserInLog($user->national_id);
            $user->position = 'IN';
            $user->save();
        }
        else
        {
            $user->position = 'IN';
            $user->save();
        }
    }

    //methods for RFID Verify Controller
    public function CreatingUserInLog($national_id)
    {
        $log = new UserLogs();
        $log->national_id = $national_id;
        $log->in = now();
        $log->gate = auth()->user()->username ?? 'API';
        $log->save();
    }
    //methods for RFID Verify Controller
    public function CreatingUserOutnLog($national_id)
    {
        $log = UserLogs::where('national_id', $national_id)->latest()->first();
        if ($log != null)
        {
            $log->out = now();
            $log->save();
        }
    }

    public function markStudentAttendance($data, $user, $lastAttendance)
    {

        $time = Carbon::now();
        $studentStart = Carbon::createFromTimeString(setting('studentStart'));
        $studentEnd = Carbon::createFromTimeString(setting('studentEnd'));
        $studentOff = Carbon::createFromTimeString(setting('studentOff'));

        // on time attendance
        if ($time < $studentStart)
        {
            $this->inAttenLog($user, $data, 1, $lastAttendance); // 1 is on time attendacne
        }
        // late attendance
        elseif ($time < $studentEnd)
        {
            $this->inAttenLog($user, $data, 2, $lastAttendance); // 2 is for late attendance
        }
        // out attendance log
        elseif ($time > $studentOff)
        {
            if ($user->lastAttendance != null)
            {
                $this->outAttendenceLog($user->lastAttendance);
            }
        }
        // after late time end no attendace mark
        else
        {
        }
    }
    public function markTeacherAttendance($data, $user, $lastAttendance)
    {

        $time = Carbon::now();
        $teacherStart = Carbon::createFromTimeString(setting('teacherStart'));
        $teacherEnd = Carbon::createFromTimeString(setting('teacherEnd'));
        $teacherOff = Carbon::createFromTimeString(setting('teacherOff'));


        // on time attendance
        if ($time < $teacherStart)
        {
            // dd('here');
            $this->inAttenLog($user, $data, 1, $lastAttendance); // 1 is on time attendacne
        }
        // late attendance
        elseif ($time < $teacherEnd)
        {

            $this->inAttenLog($user, $data, 2, $lastAttendance); // 2 is for late attendance
        }
        // out attendance log
        elseif ($time > $teacherOff)
        {

            if ($user->lastAttendance != null)
            {

                $this->outAttendenceLog($user->lastAttendance);
                $this->teacherOverTime($user, $teacherOff);
            }
        }
        // after late time end no attendace mark
        else
        {
        }
    }
    public function teacherOverTime($user, $teacherOff)
    {
        $lastAttendance =  $user->lastAttendance;
        $overTimeStartTime =  $teacherOff->copy()->addMinutes(30);

        if (now() > $overTimeStartTime)
        {
            $userOverTime = new UserOvertime();
            $userOverTime->user_id = $user->id;
            $userOverTime->attendance_id = $lastAttendance->id;
            $userOverTime->hours = $overTimeStartTime->diffInHours($lastAttendance->out);
            $userOverTime->min = $overTimeStartTime->diffInMinutes($lastAttendance->out);
            $userOverTime->save();
        }
    }


    public function markEmployeeAttendance($user, $lastAttendance)
    {

        $time = Carbon::now();
        $onTime = Carbon::createFromTimeString('11:00 AM');
        $late = Carbon::createFromTimeString('11:45 AM');
        $off = Carbon::createFromTimeString('2:00 PM');

        // on time attendance
        if ($time < $onTime)
        {
            $this->inAttenLog($user,  1, $lastAttendance); // 1 is on time attendacne
        }
        // late attendance
        elseif ($time < $late)
        {
            $this->inAttenLog($user, 2, $lastAttendance); // 2 is for late attendance
        }
        // out attendance log
        elseif ($time > $off)
        {
            if ($user->lastAttendance != null)
            {
                $this->outAttendenceLog($user->lastAttendance);
            }
        }
        // after late time end no attendace mark
        else
        {
        }
    }

    public function inAttenLog($user, $status, $lastAttendance)
    {
        // prevent multiple attendace entry for today

        if ($lastAttendance != null)
        {

            if (date('y-m-d', strtotime($lastAttendance->created_at)) != date('y-m-d'))
            {
                $attendance = new Attendance();
                $attendance->user_id = $user->id;
                $attendance->in = now();
                $attendance->status = $status;
                // $attendance->type = $user->roles[0]->name;
                // $attendance->gate = auth()->user()->username ?? 'API';

                return $attendance->save();
            }
        }
        else
        {
            $attendance = new Attendance();
            $attendance->user_id = $user->id;
            $attendance->in = now();
            $attendance->status = $status;
            // $attendance->type = $user->roles[0]->name;
            // $attendance->gate = auth()->user()->username ?? 'API';
            return $attendance->save();
        }
    }
    public function outAttendenceLog($lastAttendance)
    {
        $lastAttendance->out = now();
        $lastAttendance->save();

        $in = Carbon::createFromTimeString($lastAttendance->in);
        $out = Carbon::createFromTimeString($lastAttendance->out);

        $minutes = $out->diffInMinutes($in);

        $hours = floor($minutes / 60) . ' Hours - ' . ($minutes -   floor($minutes / 60) * 60) . ' Minutes';

        $lastAttendance->minutes = $minutes;
        $lastAttendance->worked = $hours;
        return $lastAttendance->save();
    }

    //method from RFID RfidSearchUserController
    public function activatingUser($token)
    {
        $card = RfidCard::where('token', $token)->first();
        // $card = RfidCard::where('hash_token', $token)->first();
        $card->status = 1; // Active status
        $card->saveOrFail();
    }
    public function blockingUser($token)
    {
        $card = RfidCard::where('token', $token)->first();
        // $card = RfidCard::where('hash_token', $token)->first();
        // dd($card);
        $card->status = 2; // block status
        $card->saveOrFail();
    }


    public function checkUserGate($user)
    {

        $gate = Auth()->user()->name;


        if ($gate == 'dev')
        {
            $gate = 1;
        }


        // dd($gate);

        if ($user->gates()->where('gate_id', $gate)->count() > 0)
        {
            return true;
        }
        return false;
    }
    public function checkUserAccessPoint($user)
    {
        // dd(auth()->user()->accessPointOwnerUser->id);
        $accesspoint = auth()->user()->accessPointOwnerUser;

        // dd($gate);

        if ($user->accessPoints()->where('accesspoint_id', $accesspoint->id)->count() > 0)
        {
            return true;
        }
        return false;
    }
    public function checkStudentGate($student)
    {

        $gate = Auth()->user()->name;

        if (is_int($gate))
        {
            $gate = $gate;
        }
        else
        {
            $gate = 1;
        }

        if ($student->gates()->where('gate_id', $gate)->count() > 0)
        {
            return true;
        }
        return false;
    }
    public function checkStaffGate($staff)
    {

        $gate = Auth()->user()->name;

        if (is_int($gate))
        {
            $gate = $gate;
        }
        else
        {
            $gate = 1;
        }

        if ($staff->gates()->where('gate_id', $gate)->count() > 0)
        {
            return true;
        }
        return false;
    }
}
